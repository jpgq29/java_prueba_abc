package org.juan.pablo.controllers;

import org.juan.pablo.entity.ColeccionABC;
import org.juan.pablo.services.string.TextoToArrayChar;
import org.juan.pablo.services.string.TextoUpperTrim;

import java.util.ArrayList;
import java.util.List;

public class Palabra {

    private String palabra;

    public Palabra(String palabra) {
        this.palabra = palabra;
    }

    public boolean puedo_obtener_palabra(){
        String palabraProcesada = new TextoUpperTrim(this.palabra).getTexto();
        char[] arrPalabra = new TextoToArrayChar(palabraProcesada).getTexto();
        String[][] coleccion = new ColeccionABC().getColeccion();

        String resultadoCoincidencias = "";
        List listaNegra = new ArrayList<Integer>();
        int longitudArrPalabra = arrPalabra.length;
        for (int i=0;i<longitudArrPalabra;i++){
            String seccionPalabra =  String.valueOf(arrPalabra[i]);

            int longitudColeccion = coleccion.length;
            for (int e=0;e<longitudColeccion;e++){
                if(listaNegra.contains(e)){
                    continue;
                }else{
                    Boolean coincidencia = coleccion[e][0].contains(seccionPalabra);
                    if(coincidencia){
                        listaNegra.add(e);
                        resultadoCoincidencias += seccionPalabra;
                        break;
                    }

                }
            }
        }
        return palabraProcesada.equals(resultadoCoincidencias) ? true : false;
    }
}
