package org.juan.pablo.entity;

public class ColeccionABC {
    private String[][] coleccion = {
            {"BO"},
            {"XK"},
            {"DQ"},
            {"CP"},
            {"NA"},
            {"GT"},
            {"RE"},
            {"TG"},
            {"QD"},
            {"FS"},
            {"HU"},
            {"VI"},
            {"AN"},
            {"OB"},
            {"ER"},
            {"FS"},
            {"LY"},
            {"PC"},
            {"ZM"},
            {"JW"},
    };

    public String[][] getColeccion() {
        return coleccion;
    }
}
