package org.juan.pablo.services.string;

public class TextoUpperTrim {
    private String texto;

    public TextoUpperTrim(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto.trim().toUpperCase().replace(" ","");
    }
}
