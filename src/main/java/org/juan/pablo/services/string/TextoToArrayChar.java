package org.juan.pablo.services.string;

public class TextoToArrayChar {
    private String texto;

    public TextoToArrayChar(String texto) {
        this.texto = texto;
    }

    public char[] getTexto() {
        return texto.toCharArray();
    }
}
