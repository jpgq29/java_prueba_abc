package org.juan.pablo.controllers;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;

class PalabraTest {
    @Test
    @DisplayName("Test puedo_obtener_palabra()")
    void testObtenerPalabra(){
        String palabra = "hola mndo";
        Boolean resultado = new Palabra(palabra).puedo_obtener_palabra();
        Boolean esperado = true;
        assertEquals(esperado,resultado, () -> "No se puede construir la palabra '"+palabra+"' con el diccionario de datos");
    }
}